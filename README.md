# Contours

## This project is used to generate a transparent map layer with elevation contours for Garmin devices.

## Requirements:

  * docker
  * docker-compose

## How to run:

  * file `src/contour.poly` is used to describe the map shape. Polygon files could be found at  [Geofabrik](https://download.geofabrik.de/)
  * `docker-compose up` will generate a `gmapsupp.img` file inside of 'generated' folder.
  * this file has to go to the `Garmin` folder on your device.
  * settings are at the `environment:` section in `docker-compose.yml` file.

## References

  * https://download.geofabrik.de/
  * http://katze.tfiu.de/projects/phyghtmap/
  * http://garmin.opentopomap.org/#download
  * https://github.com/der-stefan/OpenTopoMap/tree/master/garmin
