#! /bin/bash

destination="generated/$1"

mkdir -p "../$destination"
cp ../data/output/gmapsupp.img "../$destination/gmapsupp.img"

if [ $? -ne 0 ]; then
  echo "Can not copy image to $destination"
  exit 1
fi

echo '-------------------------------'
echo "Image saved to $destination"
echo '-------------------------------'

exit 0
