#! /bin/bash

if [ -z "$1" ]; then
  echo 'Error! No output dir specified!'
  exit 1
fi

mkdir -p "$1"

echo '-------------------------------'
echo 'Generation started...'
echo "Build: $1"
echo '-------------------------------'

phyghtmap --pbf \
  --jobs=$MAX_JOBS \
  --max-nodes-per-tile=500000 \
  --no-zero-contour \
  --source=view3 \
  --polygon=../src/contour.poly \
  -s $MINOR_LINE_M \
  -c $MAJOR_LINE_M,$MEDIUM_LINE_M \
  -o "$1/$MAP_ID"
