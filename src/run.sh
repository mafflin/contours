#! /bin/bash

stamp="$MAP_NAME$(date +-%F-%H_%M)"

time (
  ../src/typ_generator.sh \
    && ../src/contours_generator.sh "$stamp" \
    && ../src/template_generator.rb "$stamp" \
    && ../src/map_generator.sh "$stamp" \
    && ../src/image_saver.sh "$stamp"
)

if [ $? -ne 0 ]; then
  echo "FAILED!"
  exit 1
fi

exit 0
