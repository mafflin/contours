#! /usr/bin/ruby

MAP_DESCRIPTION = ENV['MAP_DESCRIPTION']
MAP_NAME = ENV['MAP_ID']

input_dir = ARGV[0]
input_files = `ls #{input_dir}`.split("\n")

`echo > template.args`

input_files.each_with_index do |file, index|
  `echo 'mapname: #{MAP_NAME.to_i + index}' >> template.args`
  `echo 'description: #{MAP_DESCRIPTION}' >> template.args`
  `echo 'input-file: #{input_dir}/#{file}' >> template.args`
  `echo >> template.args`
end
