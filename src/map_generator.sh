#! /bin/bash

memory_limit="-Xmx${MAX_MEMORY}M"

echo '-------------------------------'
echo 'Building map...'
echo '-------------------------------'

java $memory_limit  \
  -jar ../bin/mkgmap/mkgmap.jar \
  --style-file=../src/styles/contour/ \
  --gmapsupp \
  --max-jobs=$MAX_JOBS \
  --min-size-polygon=8 \
  --reduce-point-density=4 \
  --transparent \
  --keep-going \
  --family-id="$FAMILY_ID" \
  --draw-priority=99 \
  --area-name="$AREA_NAME" \
  --output-dir=output \
  -c template.args \
  ../data/contour.typ
