#!/bin/bash

mkdir -p generated
cp src/run.sh data/run.sh
cd data

./run.sh

if [ $? -ne 0 ]; then
  echo "Something went wrong!"
  exit 1
fi

exit 0
