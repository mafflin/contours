FROM archlinux:latest

RUN pacman -Syu --noconfirm \
  unzip curl wget mono jdk-openjdk ruby python gcc gdal \
  python-pip python-gdal python-matplotlib python-numpy \
  && pip install bs4
RUN mkdir -p /contours/bin /contours/data /contours/cache /contours/generated

ADD ./src /contours/src
COPY docker-entrypoint.sh /contours/

WORKDIR /contours/bin

RUN wget https://www.mkgmap.org.uk/download/mkgmap-r4818.zip \
  && unzip mkgmap-r4818.zip \
  && rm mkgmap-r4818.zip \
  && mv mkgmap-r4818 mkgmap
RUN wget http://katze.tfiu.de/projects/phyghtmap/phyghtmap_2.23.orig.tar.gz \
  && tar -xf phyghtmap_2.23.orig.tar.gz \
  && rm phyghtmap_2.23.orig.tar.gz \
  && mv phyghtmap-2.23 phyghtmap \
  && cd phyghtmap \
  && python setup.py install
RUN pip install lxml

WORKDIR /contours

CMD bash
